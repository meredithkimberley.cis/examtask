const alphabet = 'abcdefghijklmnopqrstuvwxyz'.split('');

const findLetters = (arr) => {
    const start = alphabet.indexOf(arr[0]);
    const end = alphabet.indexOf(arr[arr.length - 1]);
    const find = alphabet.slice(start, end + 1);

    return find.filter((letter) => !arr.includes(letter));
};

console.log(findLetters(['a', 'b', 'c', 'e', 'h']));
console.log(findLetters(['h', 'j', 'l', 'p']));
console.log(findLetters(['s', 't', 'v', 'z']));