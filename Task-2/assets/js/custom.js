jQuery(document).ready(function(){
  jQuery('.pd_t_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      // autoplay:true,
      // autoplaySpeed:2000,
      // adaptiveHeight:true,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
});

jQuery(document).ready(function(){
  jQuery('.feature_slider').slick({
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      // autoplay:true,
      // autoplaySpeed:2000,
      // adaptiveHeight:true,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 1,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
});

jQuery(document).ready(function(){
  jQuery('.our_patners_logo_slider').slick({
      slidesToShow: 4,
      slidesToScroll: 1,
      dots: false,
      // autoplay:true,
      // autoplaySpeed:2000,
      // adaptiveHeight:true,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 3,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 380,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
});

jQuery(document).ready(function(){
  jQuery('.client_review_slider_make').slick({
      slidesToShow: 3,
      slidesToScroll: 1,
      dots: false,
      // autoplay:true,
      // autoplaySpeed:2000,
      // adaptiveHeight:true,
      responsive: [
      {
        breakpoint: 992,
        settings: {
          slidesToShow: 2,
        }
      },
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 1,
        }
      }
    ]
  });
});

// fix_header
// jQuery(window).scroll(function(){
//     if (jQuery(this).scrollTop() > 50) {
//        jQuery('.top_header').addClass('fix_header');
//     } else {
//        jQuery('.top_header').removeClass('fix_header');
//     }
// });
// fix_header_end

// smooth-scrolling
jQuery(document).ready(function(){
let anchorlinks = document.querySelectorAll('a[href^="#"]')

for (let item of anchorlinks) { // relitere 
    item.addEventListener('click', (e)=> {
        let hashval = item.getAttribute('href')
        let target = document.querySelector(hashval)
        target.scrollIntoView({
            behavior: 'smooth'
        })
        history.pushState(null, null, hashval)
        e.preventDefault()
    })
}
});
// smooth-scrolling-end

// header_toggle_in_mobile
if (jQuery(window).width() < 992) {
  jQuery(document).ready(function(){
     jQuery(".top_header .navbar-toggler").click(function () {       
        jQuery(this).toggleClass("close");
        jQuery(".navbar-collapse.collapse").removeClass("show");
        jQuery(".navbar-collapse.collapse").toggleClass("visible_dropmenu");
     });
     jQuery(".top_header .nav-link").click(function () {       
        jQuery(".navbar-collapse.collapse").removeClass("visible_dropmenu");
        jQuery(".top_header .navbar-toggler").removeClass("close");
     });
  });
}
else {
}
// header_toggle_in_mobile_end